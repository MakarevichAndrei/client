//
//  ImageTableCell.h
//  client
//
//  Created by Andrei Makarevich on 3/31/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTableCell : UITableViewCell 

- (void)setTableCell:(NSString *)name thumbnailImage:(UIImage *)thumbnailImage;

@end
