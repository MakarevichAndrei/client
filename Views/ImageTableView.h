//
//  LoginView.h
//  client
//
//  Created by Andrei Makarevich on 3/30/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ImageTableCell.h"

@interface ImageTableView : UITableView

@property (strong, nonatomic) ImageTableCell *cell;

@end
