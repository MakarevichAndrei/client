//
//  ImageTableCell.m
//  client
//
//  Created by Andrei Makarevich on 3/31/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import "ImageTableCell.h"

@interface ImageTableCell()

@property (strong, nonatomic) IBOutlet UILabel *imageName;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;

@end

@implementation ImageTableCell

- (void)setTableCell:(NSString *)name thumbnailImage:(UIImage *)thumbnailImage {
    self.imageName.text = name;
    self.thumbnailImageView.image = thumbnailImage;
}

@end