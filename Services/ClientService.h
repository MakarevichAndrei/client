//
//  ClientService.h
//  client
//
//  Created by Andrei Makarevich on 4/1/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ClientServiceDelegate;

@interface ClientService : NSObject 

@property (weak, nonatomic) id<ClientServiceDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *allImagesInfo;

+ (instancetype)sharedService;

- (void)searchImages:(NSString *)searchText;
- (NSMutableArray *)getInfoForImages;

- (void)dataLoaded;

@end

@protocol ClientServiceDelegate <NSObject>

- (void)dataLoaded;

@end