//
//  ClientService.m
//  client
//
//  Created by Andrei Makarevich on 4/1/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import "ClientService.h"
#import "ImageInfo.h"

@interface ClientService () <NSXMLParserDelegate> 

@end

@implementation ClientService

#pragma mark - Lifecycle

+ (instancetype)sharedService
{
    static ClientService *clientService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        clientService = [[ClientService alloc] init];
        clientService.allImagesInfo = [[NSMutableArray alloc] init];
    });
    return clientService;
}

#pragma mark - Public

- (NSMutableArray *)getInfoForImages {
    return self.allImagesInfo;
}

- (void)searchImages:(NSString *)searchText {

    [self.allImagesInfo removeAllObjects];
    NSMutableString *searchString;
    searchString = [NSMutableString stringWithString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=c247b9f4214fee47c1cde90f51ec94ed&text="];
    if (searchText) {
        [searchString appendString:@"title%3A"];
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        [searchString appendString:searchText];
    }
    [searchString appendString:@"&privacy_filter=1&media=photos&extras=url_t%2Curl_l%2C+url_o&per_page=30&format=rest"];
    
    NSLog(@"request: %@", searchString);
    
    NSURL *url = [NSURL URLWithString:searchString];
    
    NSURLSessionDataTask *allImagesInfoDataTask = [[NSURLSession sharedSession]
                                                   dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                       NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                       NSLog(@"%@",result);
                                                       
                                                       NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];
                                                       xmlParser.delegate = self;
                                                       [xmlParser parse];
                                                   }];
    
    [allImagesInfoDataTask resume];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
}

#pragma mark - NSXMLParserDelegate

-(void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    if ([elementName isEqualToString:@"photo"]) {
        ImageInfo *imageInfo = [[ImageInfo alloc]init];
        imageInfo.imageId = attributeDict[@"id"];
        imageInfo.title = attributeDict[@"title"];
        imageInfo.thumbnailUrl = [NSURL URLWithString:attributeDict[@"url_t"]];
        NSString *largePictureUrl = attributeDict[@"url_l"];
        if (largePictureUrl) {
            imageInfo.largeUrl = [NSURL URLWithString:largePictureUrl];
        } else {
            imageInfo.largeUrl = [NSURL URLWithString:attributeDict[@"url_o"]];
        }
    
        [self.allImagesInfo addObject:imageInfo];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [self dataLoaded];
}

#pragma mark - ClientServiceDelegate

- (void)dataLoaded {
    if (self.delegate) {
        [self.delegate dataLoaded];
    }
}

@end
