//
//  Image.h
//  client
//
//  Created by Andrei Makarevich on 3/31/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageInfo : NSObject

@property (strong, nonatomic) NSString *imageId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSURL *thumbnailUrl;
@property (strong, nonatomic) NSURL *largeUrl;

@end
