//
//  ViewController.m
//  client
//
//  Created by Andrei Makarevich on 3/30/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import "TableViewController.h"
#import "ImageInfo.h"
#import "ImageTableView.h"
#import "ImageTableCell.h"
#import "ClientService.h"
#import "DetailsViewController.h"

@interface TableViewController () <UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate, ClientServiceDelegate>

@property (strong, nonatomic) IBOutlet ImageTableView *dataTable;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (copy, atomic) NSArray *allImagesInfo;
@property (strong, nonatomic) ClientService *clientService;
@property (strong, nonatomic) UIImage *fullSizePicture;
@property (strong, nonatomic) NSArray *imagesInfoBeforeSearch;
@property (weak, nonatomic) DetailsViewController *details;
@property (strong, nonatomic) NSURL *fullSizePictureURL;

@end

@implementation TableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clientService = [ClientService sharedService];
    self.clientService.delegate = self;
    [self.clientService searchImages:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.allImagesInfo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *indetifier = @"ImageTableCell";
    
    ImageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:indetifier forIndexPath:indexPath];
    
    ImageInfo *imageInfo = self.allImagesInfo[indexPath.row];
    
    NSData *imageData = [NSData dataWithContentsOfFile:documentsPathForFileName(imageInfo.thumbnailUrl)];
    
    if (imageData) {
        [cell setTableCell:imageInfo.title thumbnailImage:[UIImage imageWithData:imageData]];
    } else {
        
        [cell setTableCell:imageInfo.title thumbnailImage:[UIImage imageNamed:@"loading.png"]];
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
            void (^downloadImage)(NSURL *) = ^(NSURL *url) {
            
                NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession] downloadTaskWithURL:url
                                                                                              completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                                                                  
                                                                NSData *downloadedImagedata = [NSData dataWithContentsOfURL:location];
                                                                [downloadedImagedata writeToFile:documentsPathForFileName(url) atomically:YES];
                                                                    
                                                                UIImage *downloadedImage = [UIImage imageWithData:downloadedImagedata];
                                                                                                  
                                                               
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [cell setTableCell:imageInfo.title thumbnailImage:downloadedImage];
                                                               });
                                                           }];
            
            [downloadPhotoTask resume];
        };
        
        downloadImage(imageInfo.thumbnailUrl);    });
    
    }
    return cell;
}

NSString* (^documentsPathForFileName)(NSURL *) = ^(NSURL *url) {
    NSString *rawName = [url absoluteString];
    NSString *name = [rawName substringFromIndex:([rawName length] - 28)];
    NSString *tmpDirectory = NSTemporaryDirectory();
    return [tmpDirectory stringByAppendingPathComponent:name];
};

#pragma mark - ClientServiceDelegate

- (void)dataLoaded {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.allImagesInfo = [self.clientService getInfoForImages];
        [self.dataTable reloadData];
    });
}

#pragma mark - UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (!self.imagesInfoBeforeSearch) {
        self.imagesInfoBeforeSearch = self.allImagesInfo;
    }
    [self.clientService searchImages:searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    if (self.imagesInfoBeforeSearch) {
        self.allImagesInfo = self.imagesInfoBeforeSearch;
        [self.dataTable reloadData];
        self.imagesInfoBeforeSearch = nil;
    }
}

#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageInfo *imageInfo = self.allImagesInfo[indexPath.row];
    self.fullSizePicture = [UIImage imageNamed:@"loading.png"];
    self.fullSizePictureURL = imageInfo.largeUrl;
    
    [self performSegueWithIdentifier:@"detail" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"detail"]) {
        self.details = (DetailsViewController *) segue.destinationViewController;
        self.details.picture = self.fullSizePicture;
        self.details.url = self.fullSizePictureURL;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.searchBar resignFirstResponder];
}

@end
