//
//  DetailsViewController.m
//  client
//
//  Created by Andrei Makarevich on 4/4/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController () 

@property (strong, nonatomic) IBOutlet UIImageView *bigPicture;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bigPicture.image = self.picture;
}

-(void) viewDidAppear:(BOOL)animated {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.url) {
            self.bigPicture.image = [UIImage imageNamed:@"No_image_available.jpg"];
        } else {
            
            
            void (^downloadImage)(NSURL *) = ^(NSURL *url) {
                
                NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                               downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                                   UIImage *downloadedImage = [UIImage imageWithData:
                                                                                               [NSData dataWithContentsOfURL:location]];
                                                                   
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       self.bigPicture.image = downloadedImage;
                                                                   });
                                                               }];
                
                [downloadPhotoTask resume];
            };
        
            downloadImage(self.url);
        }
    });
}

@end
