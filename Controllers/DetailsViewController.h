//
//  DetailsViewController.h
//  client
//
//  Created by Andrei Makarevich on 4/4/16.
//  Copyright © 2016 Andrei Makarevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property (strong, nonatomic) UIImage *picture;
@property (strong, nonatomic) NSURL *url;

@end
